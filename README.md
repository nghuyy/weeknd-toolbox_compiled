#### Compiled Nokia 2780 hacking toolbox

+ Install Google fastboot on Windows/MacOS from Android SDK and Nokia Driver then enter fastboot with volume-down key pressed when plug usb cable when device off.

```shell
fastboot oem sudo
fastboot flash avb_custom_key pkmd.bin
fastboot flash vbmeta vbmeta.img
fastboot flash recovery lk2nd.img
```

+ Hold the phone volume-up key and run command

```shell
fastboot reboot
```
+ Release the volume-up key

+ Then hold volume-up key again after the "custom operating system" screen disappears

https://git.abscue.de/affe_null/weeknd-toolbox/
